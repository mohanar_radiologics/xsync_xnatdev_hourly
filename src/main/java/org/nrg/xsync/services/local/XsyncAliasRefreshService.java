package org.nrg.xsync.services.local;

/**
 * @author Mohana Ramaratnam
 *
 */
public interface XsyncAliasRefreshService {
	public void refreshToken();
}
