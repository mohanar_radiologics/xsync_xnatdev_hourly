package org.nrg.xsync.configuration.json;

import java.util.ArrayList;
import java.util.List;

import org.nrg.xsync.utils.XsyncUtils;

/**
 * @author Mohana Ramaratnam
 *
 */
public class BaseSyncConfiguration {
	
	String sync_type;
	/**
	 * @return the sync_type
	 */
	public String getSync_type() {
		return sync_type;
	}
	/**
	 * @param sync_type the sync_type to set
	 */
	public void setSync_type(String sync_type) {
		this.sync_type = sync_type;
	}

	
	

	
}
